


function cloneObj(obj1, obj2){
    for(let key in obj1){
      if (obj1[key] === 'object' && obj1[key] !== null){
        cloneObj(obj1[key]);
      }else{
        obj2[key] = obj1[key]
      }
    }
  }
  // const company = {
  //   name: 'Company name',
  //   investRating: 90,
  //   isIPO: true,
  //   divisions: 
  //   {
  //     department1: [
  //       {
  //         name: 'John',
  //         profession: 'middle SE',
  //       },
  //       {
  //         name: 'Peter',
  //         profession: 'senior SE',
  //       },
  //       {
  //         name: 'Michael',
  //         profession: 'teamlead',
  //       },
  //       {
  //         name: 'Michael',
  //         profession: 'QA',
  //       },
  //     ],
  //     department2: [
  //       {
  //         name: 'Robert',
  //         profession: 'accountant',
  //       },
  //       {
  //         name: 'Julie',
  //         profession: 'referent',
  //       },
  //     ],
  //     department3: [
  //       {
  //         name: 'Lidia',
  //         profession: 'CEO',
  //       },
  //       {
  //         name: 'Vincent',
  //         profession: 'CFO',
  //       },
  //     ],
  //   }
  // };
  
  // const user2 = {
  // };
  
  // cloneObj(company, user2);
  // console.log(company);
  // console.log(user2);
  